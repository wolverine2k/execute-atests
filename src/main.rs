use std::io::prelude::*;
use std::fs::File;
use std::fs::OpenOptions;
use std::env;
use std::path::Path;
use std::error::Error;
use std::io::BufReader;
use std::process::Command;

fn help() {
    println!("usage:
executeTests <filename>
    Execute all the AtestCases from the file. Default is masterATestList");
}

fn touch_file() {
    let output = Command::new("touch")
        .arg("allAtest.log")
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));

    if output.status.success() {
        let s = String::from_utf8_lossy(&output.stdout);
        //print!("rustc succeeded and stdout was:\n{}", s);
        write_output_to_file(s.to_string());

    } else {
        let s = String::from_utf8_lossy(&output.stdout);
        print!("rustc failed and stderr was:\n{}", s);
    }
}

fn write_output_to_file(data: String) {
    let mut file = OpenOptions::new().append(true).write(true).open("allAtest.log").unwrap();
    file.write_all(&data.as_bytes());
}

fn execute_atests(filename: String) {
    println!("filename passed is {}", filename);
    let path = Path::new(&filename);
    let display = path.display();
    // Open the path in read-only mode, returns `io::Result<File>`
    let file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that describes the error
        Err(why) => panic!("couldn't open {}: {}", display, Error::description(&why)),
        Ok(file) => file,
    };

    let reader = BufReader::new(file);
    let lines = reader.lines();

    //for (index,line) in lines.enumerate() {
    for line in lines {
        let unwrapped_line = line.unwrap();
        match unwrapped_line.chars().nth(0).unwrap_or('#') {
            '#' => {
                // This is a comment, lets skip it
            }
            _ => {
                match unwrapped_line.find(' ') {
                    None => {
                        println!("Invalid Expression {}", unwrapped_line);
                    }
                    Some(i) => {
                        let testname = format!("{}", &unwrapped_line[0..i]);
                        println!("Running Test: {}", testname);
                        let output = Command::new("atest_run")
                            .arg(testname)
                            .output()
                            .unwrap_or_else(|e| panic!("failed to execute process: {}", e));

                        if output.status.success() {
                            let s = String::from_utf8_lossy(&output.stdout);
                            //print!("rustc succeeded and stdout was:\n{}", s);

                            write_output_to_file(s.to_string());

                        } else {
                            let s = String::from_utf8_lossy(&output.stdout);
                            print!("rustc failed and stderr was:\n{}", s);
                        }
                    }
                }
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    touch_file();
    match args.len() {
        // one argument passed
        2 => {
            execute_atests(args[1].to_string());
        }
        // all the other cases
        _ => {
            // show a help message
            help();
            execute_atests("masterATestList".to_string());
        }        
    }
}